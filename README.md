# Reflexión Final

 * Lic. Martín Ceresa.
 * Departamento de Lic. en Cs. de la Computación
 * E-mail: mceresa@fceia.unr.edu.ar, martinceresa@gmail.com


## ¿Implementaría cambios en mi práctica docente? ¿Cuáles serían esos cambios?

Desde mi perspectiva, el desarrollo docente tiene un problema y una virtud. Para
mejorar como docente uno tiene que comenzar la práctica docente, e ir perfeccionándose
en ella. Es decir, el desarrollo docente tiende a mejorar pero a partir de ir cometiendo
error tras error, no sólo generando ciertos problemas para los alumnos, sino además
posiblemente desmotivando al propio docente.

Dada mi poca experiencia docente, el cambio en las diferentes metodologías y prácticas
es fundamental. Debido al cambio permanente de estudiantes, a los que tengo el 
placer de enseñar, medir la efectividad de mis métodos se vuelve una tarea extremadamente
complicada, incluyendo además que en un año me encuentro frente a estudiantes
de diferentes años académicos.

Los cambios más importantes a destacar son:
* Desarrollo de material con enfoque de taller.
* Evitar el desarrollo de clases sin intervención por parte del estudiantado.
* Motivar el libre desarrollo de los estudiantes, aunque manteniendo un lineamiento general
para los estudiantes más perdidos.
* La implementación de trabajos prácticos en vez de exámenes parciales

Los cambios en general van hacía una evaluación continua de los estudiantes, 
y dichos cambios atraerán una mayor atención (y dedicación) por parte de los docentes. Lamentablemente
dado que la falta de cargos en la facultad es una realidad palpable, en general muchos
de estos cambios se ven afectados por dicha falta, y la disponibilidad horaria. Un punto
a evaluar es la implementación de un sistema de *peer-review*, donde son los mismos
estudiantes los cuales evalúen a sus compañeros.

Muchas de éstas medidas deben implementarse teniendo dos tipos de estudiantes en mente,
uno el estudiante que podemos catalogar como avanzado, donde superará en general nuestras
expectativas (con mayor o menor esfuerzo) y el estudiante medio, que es quien no tendrá facilidad
pero si podrá alcanzar en mayor medida las expectativas docentes. Aquí no contemplo estudiantes
con bajo interés, debido a que me encuentro en años avanzados de la carrera
donde la mayoría están próximos a recibirse. Siento que es muy importante centrarnos en 
una clara evaluación para el estudiante promedio o desinteresado en mis materias, y además
poder otorgarle un amplio abanico de oportunidades al estudiante que desea ampliar su
capacitación en dichas materias.

Motivar la interacción entre la organización de las materias y los alumnos que ya la han aprobado
o cursado. Generar un ámbitos de reflexión con *feedback* directo de los estudiantes a los docentes
para mejorar en los siguientes cursos. E incrementar la interacción de estudiantes avanzados mediante
adscripciones.

## ¿Qué estrategias de enseñanza funcionan mejor con los estudiantes en mis clases?

Las mejores estrategias que mejor funcionan son:
* Trabajos Prácticos: Los alumnos tienden a desarrollarse individualmente y grupalmente. Se les
  Se les permite ampliar dichos trabajos y así explorar diferentes áreas de interés.
* Trabajo Final: Presentar un trabajo final de tema libre en vez de un examen final integrador.
* Pautas Claras sobre la evaluación: Particularmente otorgarle gran valor a promover la materia
  motiva a los estudiantes a esforzarse durante el cursado.
  
En general motivar el desarrollo independiente de los estudiantes les permite ir a su ritmo
deteniéndose en sus intereses. Las pautas claras les permiten sentirse cómodos, y al saber las 
reglas del juegos, ser conscientes de su propia evolución en el curso.

## ¿Cuáles son mis ideas sobre cómo los alumnos se apropian de los saberes?

En mi opinión, la mejor forma de permitirle a los estudiantes que se apropien de los saberes, es
mediante la experimentación. Tirarles información porque sí es un camino claramente errado que
lamentablemente es lo usual dentro de los ámbitos académicos. 

El aprendizaje no es un proceso lineal al que podemos llegar siguiendo ciertos pasos que
posiblemente a otro estudiante le haya funcionado. Cada estudiante tiene recorrer un camino,
posiblemente caótico hasta encontrar un sentido mínimo de coherencia entre lo que sabe, cree saber
y quiere aprender. Y este camino se logra recorrer experimentando dichos conocimientos, esto no significa
exámenes, sino prácticas, trabajos prácticos, etc.

## ¿Cuáles son las competencias que considera hacen posible el desarrollo responsable de la profesión docente?

La profesión docente tiene como mínimo dos requerimientos, primero el dominio del área que enseña y 
el modo en que se enseña dicha área. El docente debe saber qué enseñar y en lo posible, la mejor forma de enseñarlo.

El segundo de dichos requerimientos es el más complicado de establecer, por lo que se puede relajar a
mejorar la expresión y comunicación del docente, sumado a diferentes metodologías de enseñanza 
relacionadas al área en cuestión.

## ¿Qué aportes considera que podrían devenir de Investigar la enseñanza?

El estudio de nuevas prácticas y metodologías de enseñanzas, particular una forma concreta
de estudiar el progreso tanto de los estudiantes como de los docentes. Dado que no existe una solución a la problemática
de la enseñanza, poner como objeto de estudio cómo enseñar, y las metodologías actuales, pueden permitir el desarrollo
de las mismas o la evolución en nuevas. O bien un método para el desarrollo de métricas para la evolución de la
docencia, que se debe observar y que niveles de importancia otorgarles.
